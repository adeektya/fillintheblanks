// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'models/questions.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 5, 50, 80),
      body: SafeArea(
        child: ChangeNotifierProvider<Brain>(
          create: (context) => Brain(),
          builder: ((context, child) {
            return Consumer<Brain>(builder: (context, quest, child) {
              quest.hintDelay();
              return Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: Column(
                  children: [
                    const Center(
                      child: Text(
                        'Fill in The blanks',
                        style: TextStyle(color: Colors.white, fontSize: 24),
                      ),
                    ),
                    const SizedBox(
                      height: 120,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 28.0),
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 32),
                        alignment: Alignment.center,
                        width: double.infinity,
                        padding: const EdgeInsets.all(32),
                        decoration: BoxDecoration(
                          color: Colors.orangeAccent,
                          borderRadius: BorderRadius.circular(16),
                        ),
                        child: Text(
                          quest.getQuestion(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      height: 48,
                      child: ElevatedButton(
                        onPressed: () {
                          quest.next();
                          quest.hintDelay();
                          quest.disposeHint();
                        },
                        style: ElevatedButton.styleFrom(
                          foregroundColor: Colors.white,
                          backgroundColor: Colors.blueAccent,
                          shape: const StadiumBorder(),
                        ),
                        child: const Text(
                          'next',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 120,
                    ),
                    Container(
                      width: 100,
                      height: 48,
                      child: quest.show == true
                          ? DelayedDisplay(
                              delay: Duration(seconds: 1),
                              child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    shape: const StadiumBorder(),
                                    primary: Colors.orangeAccent,
                                  ),
                                  onPressed: () {},
                                  child: Text('Hint')))
                          : SizedBox(),
                    ),
                  ],
                ),
              );
            });
          }),
        ),
      ),
    );
  }
}
