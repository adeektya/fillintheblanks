import 'package:fill_in_blanks/models/questions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'page.dart';

void main() {
  runApp(ChangeNotifierProvider(
      create: (context) => Brain(), child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}
