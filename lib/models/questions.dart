// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:flutter/material.dart';

class Question {
  final String questionText;
  final String answer;

  Question({required this.questionText, required this.answer});
}

class Brain extends ChangeNotifier {
  List<Question> questions = [
    Question(questionText: "Flutter is owned by _____?", answer: "google"),
    Question(questionText: "Iphone is owned by ______?", answer: "Apple"),
    Question(
        questionText: "Youtube is a  _____ platform?", answer: "Streaming"),
    Question(
        questionText: "Flutter uses ______ as a language?", answer: "Dart"),
  ];

  int _qindex = 0;
  bool show = false;

  String getQuestion() {
    return questions[_qindex].questionText;
  }

  void next() {
    _qindex++;
    notifyListeners();
  }

  void hintDelay() {
    Timer(const Duration(seconds: 5), () {
      show = true;
      notifyListeners();
    });
  }

  void disposeHint() {
    show = false;
  }
}
